from mesa.visualization.ModularVisualization import ModularServer

from mesa.visualization.UserParam import UserSettableParameter

from mesa.visualization.modules import (
    CanvasGrid,
    ChartModule,
    BarChartModule,
    PieChartModule
)

from ant_simulator.agents import Ant,Feromone,Food,Nest,Wall
from ant_simulator.model import AntModel

'''
CONFIGURAZIONI GLOBALI
'''
fixed_width = 40  #number of grid cells 
fixed_height = 40 #number of grid cells
pixel_width = 600 # number of pixel 
pixel_height = 600 # number of pixel 
max_feromone_level = 40 # max weak feromone level
max_strong_level = 6 # max strong feromone level
max_feromone_boost = 1
max_strong_boost_add = 4 # start at max_feromone_boost and add this boost

#TODO calcolo quante formiche hanno trovato il cibo dopo aver seguito una scia vs quelle che l'hanno trovato a caso
#TODO parlare del fenomeno della stagnazione attorno al nido
#TODO: parlare del fatto che a volte la scia del cibo venga seguita verso la tana e non verso il cibo
#TODO: Confrontare comportamento pochi cibi grandi con tanti cibi piccoli

# Green
ANTS_FOOD_COLOR = "#F80000"
# Red
ANTS_EXPLORATION_COLOR = "#BBBBBB"

# Definizione di come la griglia deve essere visualizzata
def agent_portrayal(agent):

    if agent is None:
        return
    
    portrayal = {"Filled": "true"}

    if type(agent) is Feromone:
        if agent.level == 0:
            portrayal["Color"] = "white"
        else:
            # per ora abbiamo scelto il giallo valutiamo quale usare
            if agent.strong == False:
                portrayal["Color"] = f"rgba(255,255,0,{GetFeromoneAgentColorFromLevel(agent.level,max_feromone_level)})"
            else:
                portrayal["Color"] = f"rgba(124,252,0,{GetFeromoneAgentColorFromLevel(agent.level,max_strong_level)})"

        portrayal["Shape"] = "rect"
        portrayal["Layer"] = 0
        #portrayal["text"] = agent.level
        #portrayal["text_color"] = "Black"
        portrayal["w"] = 1
        portrayal["h"] = 1
    
    elif type(agent) is Wall:
        portrayal["Shape"] = "rect"
        portrayal["Layer"] = 1
        portrayal["Color"] = "rgba(110,37,37)"
        portrayal["w"] = 1
        portrayal["h"] = 1
    
    elif type(agent) is Food:
        portrayal["Shape"] = "circle"
        portrayal["Layer"] = 1
        portrayal["Color"] = "orange"
        portrayal["r"] = "0.5" 
    
    elif type(agent) is Nest:
        portrayal["Shape"] = "ant_simulator/Resources/nest.png"
        portrayal["Layer"] = 1
        portrayal["Color"] = "brown"
        portrayal["r"] = "0.5"

    elif type(agent) is Ant:
        portrayal["Shape"] = "ant_simulator/Resources/ant.png"
        
        if agent.food_location != None or agent.got_food: 
             portrayal["Shape"] = "ant_simulator/Resources/redAnt.png"

        portrayal["Color"] = "black"
        portrayal["Layer"] = 2

    return portrayal


grid = CanvasGrid(agent_portrayal,fixed_width,fixed_height,pixel_width, pixel_height)


model_params = {
    "width": fixed_width,
    "height": fixed_height,
    "number_of_agents": UserSettableParameter(
        "slider",
        "Number of agents",
        100, # valore default
        1, # valore minimo
        2000, #valore massimo
        1, #di quanto aumenta/dimuisce ogni step
        description="Choose how many agents include in the model"
    ),
    "fade_time": UserSettableParameter(
        "slider",
        "Steps the feromone takes to fade",
        15, # valore default
        1, # valore minimo
        50, #valore massimo
        1, #di quanto aumenta/dimuisce ogni step
        description="Choose how many steps the feromone takes to fade"
    ),
    "food_cells": UserSettableParameter(
        "slider",
        "Number of food cells",
        50, # valore default
        1, # valore minimo
        400, #valore massimo
        1, #di quanto aumenta/dimuisce ogni step
        description="Choose how many food cells include in the model"
    ),
    "food_quantity": UserSettableParameter(
        "slider",
        "Number of food pieces per cell",
        300, # valore default
        1, # valore minimo
        2000, #valore massimo
        1, #di quanto aumenta/dimuisce ogni step
        description="Choose how many food pieces that each food cell contains"
    ),
    "spawns_for_step": UserSettableParameter(
        "slider",
        "Number of ants spawned for step",
        5, # valore default
        1, # valore minimo
        100, #valore massimo
        1, #di quanto aumenta/dimuisce ogni step
        description="Choose how many ants spawn each step"
    ),
    "max_feromone_level": max_feromone_level,
    "max_strong_level": max_strong_level,
    "max_feromone_boost": max_feromone_boost,
    "max_strong_boost_add": max_strong_boost_add,
    "enable_strong_feromone": UserSettableParameter("checkbox", "Strong pheromone Enabled", True),
    "read_from_csv": UserSettableParameter("checkbox", "Use template", True)
    
}

#grafici
model_bar = BarChartModule(
    [
        {"Label": "Explorers", "Color": ANTS_EXPLORATION_COLOR},
        {"Label": "Workers", "Color": ANTS_FOOD_COLOR}
    ]
)

pie_chart = PieChartModule(
    [
        {"Label": "Explorers", "Color": ANTS_EXPLORATION_COLOR},
        {"Label": "Workers", "Color": ANTS_FOOD_COLOR}
    ]
)

step_food_pieces_chart = ChartModule(
    [
        {"Label": "Food pieces", "Color": "orange"}
    ]
)

step_workers_explorees_chart = ChartModule(
    [
        {"Label": "Explorers", "Color": ANTS_EXPLORATION_COLOR},
        {"Label": "Workers", "Color": ANTS_FOOD_COLOR}
    ]
)

# Creazione e lancio serverino
# Il modello parte con una griglia 100x100 (come sopra) e 100 agenti
server = ModularServer(AntModel,
                       [grid,step_workers_explorees_chart,step_food_pieces_chart, model_bar, pie_chart],
                       "Ant Model",
                       model_params)
server.port = 8524 # The default

# ottengo la gradazione di visibilità a partire dal livello di feromone
def GetFeromoneAgentColorFromLevel(level,max_level):

        percentageCompleted = level/max_level
        if percentageCompleted > 1:
            percentageCompleted = 1

        # 0.2 valore base di visibilità
        return 0.2 + (0.8* percentageCompleted) 
